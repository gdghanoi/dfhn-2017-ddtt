import { Component } from '@angular/core';
import { ShareService } from './services/share.service'
import { ApiService } from './services/api.service'

@Component({
    selector: 'test',
    templateUrl: 'test.component.html'
})
export class TestComponent {

    index = 0
    date

    constructor(protected share: ShareService, protected api: ApiService) {
        this.testGet()
    }

    showNotify() {
        switch (this.index % 3) {
            case 0:
                this.share.notifySuccess('This is success message')
                break
            case 1:
                this.share.notifyWarning('This is warning message')
                break
            case 2:
                this.share.notifyError('This is error message')
                break
        }
        this.index++
    }

    testGet() {
        
    }

    dateChange() {
        console.log(this.date)
    }
}
