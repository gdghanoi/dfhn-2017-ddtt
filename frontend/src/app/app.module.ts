import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'

import { appRoutes } from './app.route'

import { DaCommonModule } from './component/da-common.module'
import { HomeModule } from './container/home/home.module'

import { AppComponent } from './app.component'
import { TestComponent } from './test.component'

import { ShareService } from './services/share.service'
import { ApiService } from './services/api.service'
import { HttpModule } from '@angular/http';
import { HomeGuard } from './container/home/home.guard';
import { UserService } from './services/user.service';
import { LoginComponent } from './container/login/login.component';
import { SignupComponent } from './container/signup/signup.component';
import { OrderGuard } from './container/order/order.guard';

@NgModule({
    declarations: [
        AppComponent,
        TestComponent,
        LoginComponent,
        SignupComponent
    ],
    imports: [
        RouterModule.forRoot(appRoutes),
        HttpModule,
        DaCommonModule,
        HomeModule
    ],
    providers: [
        ShareService,
        ApiService,
        HomeGuard,
        UserService,
        OrderGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }