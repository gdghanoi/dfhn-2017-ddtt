import { NgModule } from '@angular/core';

import { DaModalComponent } from './da-modal.component';

@NgModule({
    imports: [],
    exports: [DaModalComponent],
    declarations: [DaModalComponent],
    providers: [],
})
export class DaModalModule { }
