import { Component, OnInit, ViewChild, Input, ElementRef, Output, EventEmitter } from '@angular/core';

declare var $;

@Component({
    selector: 'da-modal',
    templateUrl: 'da-modal.component.html'
})

export class DaModalComponent implements OnInit {
    @ViewChild('modal') elModal: ElementRef
    
    @Input() set open(isOpen:boolean) {
        if(isOpen == undefined) isOpen = false;
        if(isOpen == this.isOpen) return
        let command = isOpen ? 'show' : 'hide'
        $(this.elModal.nativeElement).modal(command)
    }
    @Output() openChange = new EventEmitter()

    isOpen = false;

    constructor() { }

    ngOnInit() {
        $(this.elModal.nativeElement).on('show.bs.modal', e => {
            this.isOpen = true
            this.emitOpen()
        })
        $(this.elModal.nativeElement).on('hide.bs.modal', e => {
            this.isOpen = false
            this.emitOpen()
            
        })
    }

    emitOpen() {
        this.openChange.emit(this.isOpen)

    }
    
}
