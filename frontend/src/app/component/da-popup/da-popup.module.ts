import { NgModule } from '@angular/core';

import { DaPopupComponent } from './da-popup.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule,FormsModule],
    exports: [DaPopupComponent],
    declarations: [DaPopupComponent],
    providers: [],
})
export class DaPopupModule { }

