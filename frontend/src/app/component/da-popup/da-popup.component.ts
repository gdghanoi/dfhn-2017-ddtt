import { Component, Input, Output, AfterViewInit, ElementRef, ViewChild, ViewEncapsulation, EventEmitter } from '@angular/core';
import { HostListener } from '@angular/core';

declare var jQuery: any;

@Component({
    selector: 'da-popup',
    templateUrl: './da-popup.component.html',
    styleUrls: ['./da-popup.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class DaPopupComponent implements AfterViewInit {
    @Input() triggers;

    @ViewChild('content') content: ElementRef;

    @Output() isActive = new EventEmitter();

    open = false;

    @HostListener('document:click', ['$event'])
    clickout(event) {
        let hasChild = false;
        if(this.open) {
            for(let trigger of this.triggers) {
                let trig = this.getElement(trigger)
                if (!trig.contains(event.target)) {
                    let boo = event.path.map(it => jQuery(it).attr('class')).join(' ').includes('tooltipster-content')
                    if (boo) {
        
                    } else {
                        this.closeTooltip()
                    }
                }
            }
        }
    }

    constructor() {
        this.triggers = []
    }

    ngAfterViewInit() {
        for (const trigger of this.triggers) {
            this.initTooltip(trigger);
        }
        jQuery.tooltipster.on('before', (event) => {
            jQuery('.da-is-tooltipster').tooltipster('close');
        });
    }

    initTooltip(triggerIp) {
        let trigger = triggerIp.el ? triggerIp.el.nativeElement : triggerIp;
        jQuery(trigger).addClass('da-is-tooltipster');
        let options = {
            content: this.content.nativeElement,
            contentCloning: false,
            theme: ['tooltipster-shadow', 'tooltipster-search-box'],
            interactive: true,
            trigger: 'custom',
            triggerOpen: {
                click: true,
            },
            triggerClose: {
                click: true,
                originClick: true
            },
            side: ['bottom', 'top', 'right', 'left'],
            delay: 10,
        }
        let tooltip = jQuery(trigger).tooltipster(options).tooltipster('instance');
        tooltip.on('created', (event) => {
            this.toggleTooltip(true);
        }).on('close', (event) => {
            this.toggleTooltip(false);
        });
    }

    getElement(obj) {
        return obj.el ? obj.el.nativeElement : obj
    }

    closeTooltip() {
        for (const trigger of this.triggers) {
            jQuery(this.getElement(trigger)).tooltipster('close');
        }
    }

    toggleTooltip(isOpen) {
        this.open = isOpen
        this.isActive.emit(isOpen);
    }
}
