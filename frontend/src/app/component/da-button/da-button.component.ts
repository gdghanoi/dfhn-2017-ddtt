import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

@Component({
    selector: 'da-button',
    templateUrl: 'da-button.component.html'
})

export class DaButtonComponent implements OnInit {

    @ViewChild('el') el: ElementRef;

    constructor() { }

    ngOnInit() { }
}