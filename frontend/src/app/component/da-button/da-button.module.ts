import { NgModule } from '@angular/core';

import { DaButtonComponent } from './da-button.component';

@NgModule({
    imports: [],
    exports: [DaButtonComponent],
    declarations: [DaButtonComponent],
    providers: [],
})
export class DaButtonModule { }
