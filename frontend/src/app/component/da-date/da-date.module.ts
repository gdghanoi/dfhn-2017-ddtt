import { NgModule } from '@angular/core';

import { DaDateComponent } from './da-date.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule,FormsModule],
    exports: [DaDateComponent],
    declarations: [DaDateComponent],
    providers: [],
})
export class DaDateModule { }
