import {
    Component, Input,
    Output,
    EventEmitter,
    AfterViewInit,
    ElementRef,
    ViewChild,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import { ValueTransformer } from '@angular/compiler/src/util';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'da-date',
    templateUrl: 'da-date.component.html',
    styleUrls: ['da-date.css']
})
export class DaDateComponent {
    @ViewChild('elInput') elInput: ElementRef
    @Input() value
    @Output() valueChange = new EventEmitter()

    constructor() {

    }

    ngOnInit() {
        let self = this
        $(this.elInput.nativeElement).datepicker({
            format: 'yyyy-mm-dd'
        }).on('changeDate', function(e) {
           let date = moment(new Date(this.value)).format('YYYY-MM-DD')
           self.valueChange.emit(date)
        });
    }
}
