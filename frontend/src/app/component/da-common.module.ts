import { NgModule } from '@angular/core';
import { DaButtonModule } from './da-button/da-button.module';
import { DaModalModule } from './da-modal/da-modal.module';
import { DaPopupModule } from './da-popup/da-popup.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { DaDateModule } from './da-date/da-date.module';

@NgModule({
    imports: [],
    exports: [
        DaButtonModule,
        DaModalModule,
        DaPopupModule,
        FormsModule,
        BrowserModule,
        DaDateModule
    ],
    declarations: [],
    providers: [],
})
export class DaCommonModule { }
