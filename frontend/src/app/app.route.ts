import { RouterModule, Routes } from '@angular/router'
import { AppComponent } from './app.component'
import { testRoute } from './test.route'
import { homeRoute } from './container/home/home.route'
import { LoginComponent } from './container/login/login.component';
import { SignupComponent } from './container/signup/signup.component';

export const appRoutes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {path: '', redirectTo: 'app', pathMatch: 'full'},
            { path: 'login', component: LoginComponent },
            { path: 'signup', component: SignupComponent },
            ...homeRoute,
        ]
    },
];