import { Injectable } from "@angular/core";

declare var moment

@Injectable()
export class UserService {
    date = moment(new Date()).format('YYYY-MM-DD')
    meal = 'lunch'
    table
}