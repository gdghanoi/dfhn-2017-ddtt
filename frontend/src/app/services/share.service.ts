import { Injectable } from "@angular/core";

declare var $

@Injectable()
export class ShareService {
    notifySuccess(message) {
        $.notify({
            message: message,
            icon: 'fa fa-check'
        }, {
            type: 'success',
            animate: {
                enter: 'animated bounceInRight',
                exit: 'animated bounceOutRight'
            },
            z_index: 99999,
        });
    }

    notifyWarning(message) {
        $.notify({
            message: message,
            icon: 'fa fa-check'
        }, {
            type: 'warning',
            animate: {
                enter: 'animated bounceInRight',
                exit: 'animated bounceOutRight'
            },
            z_index: 99999,
        });
    }

    notifyError(message) {
        $.notify({
            message: message,
            icon: 'fa fa-exclamation-triangle'
        }, {
            type: 'danger',
            animate: {
                enter: 'animated wobble',
                exit: 'animated bounceOutRight'
            },
            z_index: 99999,
        });
    }

    getDate(str) {
        let string: String = str
        if(string) {
            return string.substring(0,10)
        } else {
            return ''
        }
        
    }
}