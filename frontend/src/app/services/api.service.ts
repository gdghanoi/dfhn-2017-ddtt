import { Injectable } from "@angular/core";
import { Http, Headers, Response } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";

declare var _

@Injectable()
export class ApiService {
    enpoint = "http://localhost:8000/api/"

    current_path: string;
    constructor(private http: Http, private router: Router) {

    }

    get(option) {
        let method = option.method
        let prefix = ""
        if(option.main) {
            prefix = this.enpoint
        }
        let url = prefix + option.url
        let header = this.getHeader(option)
        let param = { headers: header, search: option.params }
        switch(method) {
            case 'get': return this.http.get(url, param)
            case 'post': return this.http.post(url,option.params,{headers: header})
        }
        return this.http.get(url, param)
    }

    private getHeader(option) {
        let contentHeaders = new Headers()
        if (!option.headers) option.headers = []
        if (!_.find(option.headers, e => e[0] == "Accept")) {
            option.headers.push(['Accept', 'application/json'])
        }
        if (!_.find(option.headers, e => e[0] == "Content-Type")) {
            option.headers.push(['Content-Type', 'application/json'])
        }
        if (option.token) {
            let token = localStorage.getItem('token')
            if (token) {
                contentHeaders.append('Authorization', 'Bearer' + ' ' + token)
            } else {
                
            }
                
        }
        return contentHeaders
    }

    private checkResponse(error: any) {
        switch (error.status) {
            case 419:
                alert('Your session expired! Please log in again!');
                localStorage.removeItem('jwt');
                this.current_path = this.router.routerState.snapshot.url;
                this.router.navigate(['/login']);
                // APIService.isChecking = false;                
                break;

            case 401:
                this.router.navigate(['/login']);

            case 403:                                      
                break;
        }
    }
}