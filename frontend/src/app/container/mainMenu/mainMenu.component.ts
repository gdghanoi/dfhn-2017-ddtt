import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Order } from './order';
import { HomeService } from '../home/home.service';
import { ApiService } from '../../services/api.service';
import { ShareService } from '../../services/share.service';
import { Router } from '@angular/router'

declare var $

@Component({
    selector: 'mainMenu',
    templateUrl: 'mainMenu.component.html',
    styleUrls: ['mainMenu.component.css']
})

export class MainMenuComponent implements OnInit {
    orders = []
    modalIsOpen = false;
    selectedOrder: Order;

    constructor(protected home: HomeService, protected api: ApiService, protected share:ShareService, protected route: Router) {
        this.home.showMenu = true;
        this.home.title = "Your orders"
        this.home.back = "/app/main"
        this.home.isHome = true
    }

    ngOnInit() {
        this.getBooking()
    }

    getBooking() {
        this.orders = []
        this.api.get({
            main: true,
            url: 'bookings',
            method: 'get',
            token: true,
        }).subscribe(success=>{
            this.orders = success.json().data
        }, error => {

        })
    }
    
    onSelect(order: Order): void {
        this.selectedOrder = order;
        this.modalIsOpen = true;
    }

    ngOnDestroy() {
        this.home.showMenu = false
        this.home.isHome = false
    }

    goTable() {
        this.route.navigate(['/app/tables'])
    }
}