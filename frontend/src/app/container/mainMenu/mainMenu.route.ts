import { MainMenuComponent } from './mainMenu.component'

export const mainMenuRoute = [
    { path: 'main', component: MainMenuComponent }
]