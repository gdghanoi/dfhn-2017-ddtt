import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service'

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})
export class LoginComponent {

    name
    password

    resultType = 'danger'
    resultMessage

    constructor(protected route: Router, protected api: ApiService) {

    }

    login() {
        this.api.get({
            main: true,
            url: 'authenticate',
            method: 'post',
            params: {
                email: this.name,
                password: this.password
            }
        }).subscribe(result => {
            let token = result.json().token
            localStorage.setItem('token', token)
            this.route.navigate(['app/main'])
        }, error => {
            const res = error.json()
            this.resultType = 'danger'
            this.resultMessage = res.message
        })
    }

    goSignup(e) {
        e.preventDefault()
        e.stopPropagation()
        this.route.navigate(['signup'])
    }
}
