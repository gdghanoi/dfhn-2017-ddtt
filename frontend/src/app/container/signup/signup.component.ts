import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { ShareService } from '../../services/share.service';

@Component({
    selector: 'login',
    templateUrl: 'signup.component.html',
    styleUrls: ['signup.component.css']
})
export class SignupComponent {
    name
    email
    phone
    password
    password_confirmation

    errors = {}

    resultMessage = ''

    constructor(protected route: Router, protected api: ApiService, protected share: ShareService) {

    }

    signUp() {
        this.api.get({
            main: true,
            url: 'register',
            method: 'post',
            params: {
                name: this.name,
                email: this.email,
                phone: this.phone,
                password: this.password,
                password_confirmation: this.password_confirmation
            }
        }).subscribe(result => {
            const res = result.json()
            this.resultMessage = res.message
        }, error => {
            const res = error.json()
            this.errors = res.errors || {}
        })
    }

    goLogin(e) {
        e.preventDefault()
        e.stopPropagation()
        this.route.navigate(['/login'])
    }
}
