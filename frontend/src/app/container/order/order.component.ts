import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { HomeService } from '../home/home.service';
import { ApiService } from '../../services/api.service';
import { UserService } from '../../services/user.service';
import { ShareService } from '../../services/share.service';

@Component({
    selector: 'order',
    templateUrl: 'order.component.html'
})
export class OrderComponent {
    selectedOrder: any;
    isModalOpen = false
    foods = []

    constructor(protected route: Router, protected homeSv: HomeService, protected api: ApiService, protected user: UserService, protected share: ShareService) {
        this.homeSv.showMenu = true
        this.homeSv.title = "Confirmation"
        this.homeSv.back = '/app/tables'

        this.api.get({
            main: true,
            method: 'get',
            token: true,
            url: 'foods'
        }).subscribe(result => {
            this.foods = result.json().data
            for(let food of this.foods) {
                food.quantity = 0
            }
        }, error => {

        })

        this.selectedOrder = this.user.table
    }

    ngOnDestroy() {
        this.homeSv.showMenu = false
    }

    edit() {
        this.isModalOpen = true
    }

    sum() {
        let sum = 0
        for(let food of this.foods) {
            sum += food.price * food.quantity
        }
        return sum
    }

    book() {
        let food = this.foods.map(arr => { return {id: arr.id, quantity: arr.quantity }})
        this.api.get({
            main: true,
            url: 'bookings',
            method: 'post',
            token: true,
            params: {
                table_id: this.user.table.id,
                date_time: this.user.date,
                meal: this.user.meal,
                foods: food
            }
        }).subscribe(success=>{
            this.share.notifySuccess(success.json().message)
            this.route.navigate(['/app/main']);
        }, error => {
            this.route.navigate(['/app/main']);
        })
    }
}
