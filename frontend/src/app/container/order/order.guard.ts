import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router'
import { UserService } from '../../services/user.service';

@Injectable()
export class OrderGuard implements CanActivate {

    constructor(protected api:ApiService, protected route: Router, protected user: UserService) {

    }

    canActivate() {
        if(!this.user.table) {
            this.route.navigate(['/app/tables'])
            return false
        }
        return true
    }
}