export class Table {
    name: string;
    floor: string;
    numberOfSeat: number;
    description: string;
    available: boolean;
}