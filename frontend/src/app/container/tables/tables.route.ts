import { TablesComponent } from './tables.component'

export const tablesRoute = [
    {path: 'tables', component: TablesComponent}
]