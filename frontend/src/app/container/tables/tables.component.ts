import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Table } from './table';
import { UserService } from '../../services/user.service';
import { HomeService } from '../home/home.service';
import { ApiService } from '../../services/api.service';
import { ShareService } from '../../services/share.service';
import { Router } from '@angular/router'

declare var $

@Component({
    selector: 'tables',
    templateUrl: 'tables.component.html',
    styleUrls: ['tables.component.css']
})

export class TablesComponent implements OnInit {
    tables
    modalIsOpen = false
    canGo = false

    constructor(protected user: UserService, protected home: HomeService, protected api: ApiService, protected share: ShareService, protected route: Router) {
        this.home.showMenu = true
        this.home.title = "Table"
        this.home.back = "/app/main"
    }

    ngOnInit() {
        this.getTable()
    }

    getTable() {
        this.tables = []
        this.user.table = undefined
        this.api.get({
            main: true,
            method: 'get',
            url: 'tables',
            token: true,
            params: {
                date: this.user.date,
                meal: this.user.meal
            }
        }).subscribe(result => {
            this.tables = result.json().data
        },error => {

        })
    }

    checkStatus(table) {
        return table.status == 'available'
    }

    onSelect(table: Table): void {
        if (this.checkStatus(table)) {
            this.modalIsOpen = true;
            this.user.table = table
        }
    }

    refresh() {
        this.getTable()
    }

    book() {
        // this.api.get({
        //     main: true,
        //     url: 'bookings',
        //     method: 'post',
        //     token: true,
        //     params: {
        //         table_id: this.user.table.id,
        //         date_time: this.user.date,
        //         meal: this.user.meal
        //     }
        // }).subscribe(success=>{
        //     this.share.notifySuccess(success.json().message)
        //     this.modalIsOpen = false
        //     this.getTable()
        // }, error => {

        // })
        this.modalIsOpen = false
        this.canGo = true
        
    }

    goOrder() {
        if(this.canGo) this.route.navigate(['/app/order'])
    }

    ngOnDestroy() {
        this.home.showMenu = false
    }
}