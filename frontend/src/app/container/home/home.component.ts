import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HomeService } from './home.service'
import { Router } from '@angular/router'

declare var $

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit {

    constructor(protected sv: HomeService, protected route: Router) { }

    ngOnInit() {
    
    }

    back() {
        this.route.navigate([this.sv.back])
    }

    out() {
        localStorage.removeItem('token')
        this.route.navigate(['/login'])
    }
}