import { HomeComponent } from './home.component'
import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from '../login/login.component'
import { TablesComponent } from '../tables/tables.component';
import { SignupComponent } from '../signup/signup.component';
import { MainMenuComponent } from '../mainMenu/mainMenu.component';
import { HomeGuard } from './home.guard';
import { OrderComponent } from '../order/order.component';
import { OrderGuard } from '../order/order.guard';

export const homeRoute: Routes = [
    {
        path: 'app',
        component: HomeComponent,
        canActivate: [HomeGuard],
        children: [
            { path: '', redirectTo: 'main', pathMatch: 'full' },
            { path: 'tables', component: TablesComponent },
            { path: 'main', component: MainMenuComponent },
            { path: 'order', component: OrderComponent, canActivate: [OrderGuard] }
        ]
    }
]
