import { NgModule } from '@angular/core'

import { DaCommonModule } from '../../component/da-common.module'
import { HomeComponent } from './home.component'
import { RouterModule } from '@angular/router'
import { appRoutes } from '../../app.route'
import { HomeService } from './home.service'
import { TablesComponent } from '../tables/tables.component';
import { MainMenuComponent } from '../mainMenu/mainMenu.component';
import { OrderComponent } from '../order/order.component';

@NgModule({
    imports: [
        DaCommonModule,
        RouterModule.forRoot(appRoutes),
    ],
    exports: [],
    declarations: [
        HomeComponent,
        TablesComponent,
        MainMenuComponent,
        OrderComponent
    ],
    providers: [HomeService],
})
export class HomeModule { }
