import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router'

@Injectable()
export class HomeGuard implements CanActivate {

    constructor(protected api:ApiService, protected route: Router) {

    }

    canActivate() {
        let token = localStorage.getItem('token')
        if(!token) {
            this.route.navigate(['/login'])
            return false
        }
        if (token) {
            this.api.get({
                main: true,
                url: 'me',
                method: 'get',
                token: true
            }).subscribe(re => {
                
            }, err => {
                this.route.navigate(['/login'])
            })
        }
        return true
    }
}