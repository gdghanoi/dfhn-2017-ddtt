import { Injectable } from "@angular/core";

@Injectable()
export class HomeService {
    showMenu = false
    title = ""
    back = "/"
    isHome = false
}