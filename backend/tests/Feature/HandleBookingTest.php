<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Booking;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingTest extends TestCase
{
    use RefreshDatabase;

    public function testHandleBookingSuccessful()
    {
      $booking = Booking::create([
        'user_id' => $this->user->id,
        'table_id' => 1,
        'date_time' => '2017-01-01 00:00:00',
        'status' => 'booking'
      ]);

      $response = $this->withHeaders($this->headers($this->admin))->json('post', '/api/bookings/' . $booking->id . '/handle', [
          'status' => 'approved'
      ]);

      $response
          ->assertStatus(200)
          ->assertJsonStructure([
              'status_code', 'message', 'data' => [
                  'user_id',
                  'table_id',
                  'date_time',
                  'status',
                  'payment_method',
                  'paid',
                  'note',
              ]
          ])
          ->assertJson([
              'data' => [
                  'table_id' => 1,
                  'status' => 'proved'
              ]
          ]);
    }

    public function testHandleBookingAsUserFailed()
    {
        $response = $this->withHeaders($this->headers($this->user))->json('post', '/api/bookings/1/handle', [
          'status' => 'proved'
        ]);

        $response
            ->assertStatus(401)
            ->assertJson([
                'message' => 'You do not have permission to handle booking'
            ]);
    }
}
