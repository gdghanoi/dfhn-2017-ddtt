<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TableTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAllTablesSuccessful()
    {
        $response = $this->withHeaders($this->headers($this->user))->json('get', '/api/tables');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'status_code', 'message', 'data' => [
                    [
                        'name',
                        'status',
                        'location',
                        'number_of_seats',
                        'vip',
                        'description',
                        'note',
                    ]
                ]
            ]);
    }
}
