<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingTest extends TestCase
{
    use RefreshDatabase;

    public function testBookingTablesSuccessful()
    {
        $user = User::find(2);

        $response = $this->withHeaders($this->headers($user))->json('post', '/api/bookings', [
            'table_id' => 1,
            'payment_method' => 'card',
            'paid' => true,
            'note' => 'This is note'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'status_code', 'message', 'data' => [
                    'user_id',
                    'table_id',
                    'date_time',
                    'status',
                    'payment_method',
                    'paid',
                    'note',
                ]
            ])
            ->assertJson([
                'data' => [
                    'user_id' => $user->id,
                    'table_id' => 1,
                    'status' => 'booking',
                    'payment_method' => 'card',
                    'paid' => 1,
                    'note' => 'This is note'
                ]
            ]);
    }
}
