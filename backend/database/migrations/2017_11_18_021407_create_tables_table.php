<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('status')->nullable(); // unvaialble, available
            $table->string('location')->nullable();
            $table->integer('number_of_seats')->default(0);
            $table->boolean('vip')->default(false);
            $table->text('description')->nullable(); // describes vip location
            $table->text('note')->nullable(); // reason for disable
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
