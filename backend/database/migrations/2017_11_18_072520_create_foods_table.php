<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->string('name');
            $table->integer('price');
            $table->string('note');
            $table->string('status');//available - unavailable
            $table->timestamps();
        });

        Schema::create('booking_food', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id');
            $table->foreign('booking_id')
                ->references('id')
                ->on('bookings')
                ->onDelete('SET NULL');
            $table->integer('food_id');
            $table->foreign('food_id')
                ->references('id')
                ->on('foods')
                ->onDelete('SET NULL');
            $table->integer('quantity');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_food');
        Schema::dropIfExists('foods');
    }
}
