<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('table_id')->unsigned();            
            $table->dateTime('date_time');
            $table->string('meal');//lunch, dinner
            $table->string('status'); // booking, declined, proved
            $table->string('payment_method')->default('cash');
            $table->boolean('paid')->default(false);
            $table->text('description')->nullable(); // user more request info
            $table->text('note')->nullable(); // desclined reason
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('SET NULL');

            $table->foreign('table_id')
                ->references('id')
                ->on('tables')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('bookings');
    }
}
