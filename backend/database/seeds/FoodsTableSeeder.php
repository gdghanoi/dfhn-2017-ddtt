<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $foods = [
            [
                'category' => 'dish',
                'name' => 'Gà chiên đắng',
                'price' => '100000',
                'status' => 'available',
                'note' => 'Ngon nhất vịnh bắc bộ',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'category' => 'dish',
                'name' => 'Gà không lối ',
                'price' => '50000',
                'status' => 'available',
                'note' => 'Ngon nhì vịnh bắc bộ',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'category' => 'buffet',
                'name' => 'Combo sinh viên',
                'price' => '200000',
                'status' => 'available',
                'note' => 'Ngon bổ rẻ',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'category' => 'buffet',
                'name' => 'Combo gia đình',
                'price' => '200000',
                'status' => 'available',
                'note' => 'Béo tròn',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('foods')->insert($foods);

        $booking_food = [
            [
                'booking_id' => 1,
                'food_id' => 1,
                'quantity' => 1
            ],
            [
                'booking_id' => 1,
                'food_id' => 2,
                'quantity' => 2
            ],
            [
                'booking_id' => 1,
                'food_id' => 1,
                'quantity' => 1
            ],
            [
                'booking_id' => 2,
                'food_id' => 3,
                'quantity' => 1
            ],
            [
                'booking_id' => 3,
                'food_id' => 4,
                'quantity' => 1
            ]
        ];
        DB::table('booking_food')->insert($booking_food);

    }
}
