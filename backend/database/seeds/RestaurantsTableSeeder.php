<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RestaurantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'name' => 'DDTT Restaurant',
            'email' => 'ddtt@restaurant.com',
            'phone' => '0123456789',
            'location' => '21.004671,105.8449383',
            'address' => 'Tạ Quang Bửu, Hai Bà Trưng, Hà Nội',
            'description' => 'Quán ăn phục vụ các món đặc sản vùng đồng bằng Sông Hồng',
        ]);
    }
}
