<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $booking = [
            [
                'user_id' => 2,
                'table_id' => 1,
                'date_time' => '2017-11-19 12:20:10',
                'meal' => 'lunch',
                'status' => 'booking',
                'payment_method' => 'cash',
                'note' => 'test1',
                'paid' => false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user_id' => 2,
                'table_id' => 2,
                'date_time' => '2017-11-19 19:20:10',
                'meal' => 'dinner',
                'status' => 'approved',
                'payment_method' => 'cash',
                'note' => 'test1',
                'paid' => false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'user_id' => 2,
                'table_id' => 2,
                'date_time' => '2017-11-19 19:20:10',
                'meal' => 'dinner',
                'status' => 'declined',
                'payment_method' => 'cash',
                'note' => 'test1',
                'paid' => false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('bookings')->insert($booking);
    }
}
