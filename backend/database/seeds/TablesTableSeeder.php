<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tables')->insert([
            [ 'name' => '01', 'status' => 'empty', 'location' => 'Floor 01', 'number_of_seats' => '2', 'vip' => false, 'description' => 'Bàn ăn sạch sẽ, có thể nhìn ra đường', 'note' => '', ],
            [ 'name' => '02', 'status' => 'full', 'location' => 'Floor 01', 'number_of_seats' => '2', 'vip' => false, 'description' => 'Bàn ăn sạch sẽ, có thể nhìn ra đường', 'note' => '', ],
            [ 'name' => '03', 'status' => 'full', 'location' => 'Floor 01', 'number_of_seats' => '2', 'vip' => false, 'description' => 'Bàn ăn sạch sẽ, có thể nhìn ra đường', 'note' => '', ],
            [ 'name' => '04', 'status' => 'unavaiable', 'location' => 'Floor 01', 'number_of_seats' => '4', 'vip' => false, 'description' => 'Bàn ăn sạch sẽ', 'note' => 'Vị trí đang được sửa chữa', ],
            [ 'name' => '05', 'status' => 'empty', 'location' => 'Floor 01', 'number_of_seats' => '4', 'vip' => false, 'description' => 'Bàn ăn sạch sẽ', 'note' => '', ],
            [ 'name' => '06', 'status' => 'empty', 'location' => 'Floor 02', 'number_of_seats' => '4', 'vip' => true, 'description' => 'Cảnh đẹp và thoáng mát', 'note' => '', ],
            [ 'name' => '07', 'status' => 'full', 'location' => 'Floor 02', 'number_of_seats' => '4', 'vip' => true, 'description' => 'Cảnh đẹp và thoáng mát', 'note' => '', ],
            [ 'name' => '08', 'status' => 'full', 'location' => 'Floor 02', 'number_of_seats' => '4', 'vip' => true, 'description' => 'Cảnh đẹp và thoáng mát', 'note' => '', ],
            [ 'name' => '09', 'status' => 'full', 'location' => 'Floor 02', 'number_of_seats' => '8', 'vip' => true, 'description' => 'Phòng kín, rộng rãi với 6 chỗ ngồi, phục vụ chu đáo', 'note' => '', ],
            [ 'name' => '10', 'status' => 'full', 'location' => 'Floor 02', 'number_of_seats' => '8', 'vip' => true, 'description' => 'Phòng kín, rộng rãi với 6 chỗ ngồi, phục vụ chu đáo', 'note' => '', ],
        ]);
    }
}
