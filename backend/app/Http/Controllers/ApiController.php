<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
  public function response ($data = null, $status = 200, $message = 'Successful') {
      return response()->json([
        'status_code' => $status,
        'message' => $message,
        'data' => $data
      ], $status);
  }

  public function responseError ($status, $message, $errors = null) {
    return response()->json([
      'status_code' => $status,
      'message' => $message,
      'errors' => $errors
    ], $status);
  }
}
