<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\HandleBookingRequest;
use Illuminate\Http\Request;
use App\Table;
use App\Booking;
use Carbon\Carbon;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class BookingController extends ApiController
{
    public function index (Request $request) {
        $user = $request->user();

        if ($user->admin) {
            return $this->response(Booking::query()->with('foods')->get());
        } else {
            return $this->response(Booking::query()->with('foods')->where('user_id', $user->id)->get());
        }
    }

    public function store (BookingRequest $request) {
        $user = $request->user();

        $tableId = $request->input('table_id');
        $dateTime = $request->input('date_time');
        $meal = $request->input('meal');
        $paymentMethod = $request->input('payment_method', 'cash');
        $paid = $request->input('paid', false);
        $note = $request->input('note');
        $foods = $request->input('foods');

        $count = Booking::query()
            ->whereDate('date_time', '=', $dateTime)
            ->where('meal', '=', $meal)
            ->where('table_id', '=', $tableId)
            ->where('status','=','approved')->count();
        if ($count > 0) {
            return $this->responseError(433, 'The table unavailable to booking, please choose another table!');
        }

        $booking = Booking::create([
            'user_id' => $user->id,
            'table_id' => $tableId,
            'date_time' => $dateTime,
            'meal' => $meal,
            'status' => 'approved',
            'payment_method' => $paymentMethod,
            'paid' => $paid,
            'note' => $note
        ]);
        if ($foods) {
            foreach ($foods as $food) {
                $booking->foods()->attach($food['id'],array('quantity' => $food['quantity']));
            }
            $booking->save();
            $booking->foods;
        }


        return $this->response($booking);
    }

    public function handle (HandleBookingRequest $request, $id) {
        $user = $request->user();

        if (!$user->admin) {
            return $this->responseError(401, 'You do not have permission to handle booking');
        }

        $booking = Booking::findOrFail($id);
        $status = $request->input('status');
        $note = $request->input('note');
        
        $booking->status = $status;
        
        if ($note) {
            $booking->note = $note;
        }

        $booking->save();

        return $this->response($booking);
    }
}
