<?php

namespace App\Http\Controllers\Api;

use App\Booking;
use App\Http\Controllers\ApiController;
use App\Table;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TableController extends ApiController
{
    public function index (Request $request) {
//        $date = Carbon::createFromFormat('Y-m-d', $request->input('date'));
        $meal = $request->input('meal');

        $pendingTables = Booking::query()->select(['table_id','status'])->whereDate('date_time', '=', $request->input('date'))->where('meal', '=', $meal)->get();
        $tables = Table::query()->get();

        foreach ($tables as $table) {
            $table->status = 'available';

            foreach ($pendingTables as $tb2) {
                if ($table->id == $tb2->table_id) {
                    if ($tb2->status == 'approved')
                        $table->status  = 'unavailable';
                }
            }
        }

        return $this->response($tables);
    }
}
