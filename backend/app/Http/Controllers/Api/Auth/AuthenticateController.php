<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Access\User\UpdateUserPasswordRequest;
use App\Http\Requests\Api\Auth\ChangePasswordRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthenticateController extends Controller
{
    use Helpers;


    public function __construct()
    {
    }

    /**
     *  API Login, on success return JWT Auth token
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'code' => 'invalid_credentials',
                    'status_code' => 401,
                    'message' => 'Please try again or reset the password'
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'code' => 'could_not_create_token',
                'status_code' => 500,
                'message' => 'Could not create token'
            ], 500);
        }

        // Check user
        $user = User::query()->where('email', $request->input('email'))->first();

        return response()->json(compact('token'));
    }


    public function logout()
    {
        $token = JWTAuth::getToken();
        JWTAuth::invalidate($token);


        return response()->json([
            'status_code' => 200,
            'message' => 'Successful logout'
        ], 200);
    }


    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json([
                    'code' => 'user_not_found',
                    'status_code' => 404,
                    'message' => 'User not found'
                ], 404);
            }

            return response()->json(compact('user'));

        } catch (TokenExpiredException $e) {
            return response()->json([
                'code' => 'token_expired',
                'status_code' => $e->getStatusCode(),
                'message' => 'Token expired'
            ], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json([
                'code' => 'token_invalid',
                'status_code' => $e->getStatusCode(),
                'message' => 'Token invalid'
            ], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json([
                'code' => 'token_absent',
                'status_code' => $e->getStatusCode(),
                'message' => 'Token absent'
            ], $e->getStatusCode());
        }

    }


    public function refreshToken()
    {
        $token = JWTAuth::getToken();

        if (!$token) {
            return $this->response->errorMethodNotAllowed('Token not provided');
        }

        try {
            $refreshedToken = JWTAuth::refresh($token);
        } catch (JWTException $e) {
            return $this->response('Unable to refresh Token');
            throw $e;
        }

        return $this->response->withArray(['token' => $refreshedToken]);
    }

    public function checkToken()
    {
        return $this->response->noContent(); // Middleware api.auth will handle logic for this method
    }

    public function register(RegisterRequest $request)
    {
        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));

        $user = User::query()->create([
            'name' => $name,
            'phone' => $phone,
            'admin'=> false,
            'email' => $email,
            'password'=> $password]);
        return response()->json([
            'status_code' => 201,
            'message' => 'Successfully Registered',
            'data' => $user
        ], 201);
    }
}
