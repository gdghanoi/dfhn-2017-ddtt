<?php

namespace App\Http\Controllers\Api;

use App\Food;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class FoodController extends ApiController
{
//    public function index()
//    {
//        $dish =Food::query()->where('status','=','available')
//            ->where('category','=','dish')->get();
//        $buffet =Food::query()->where('status','=','available')
//            ->where('category','=','buffet')->get();
//        return $this->response(['dish'=> $dish, 'buffet' => $buffet]);
//    }

    public function index()
    {
        return $this->response(Food::query()->where('status','=','available')->get());
    }
}
