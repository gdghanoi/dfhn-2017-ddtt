<?php

namespace App\Http\Requests;

use Dingo\Api\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'table_id' => 'required|numeric|exists:tables,id',
            'date_time' => 'required',
            'meal' => 'required|in:lunch,dinner',
            'payment_method' => 'nullable|in:cash,card'
        ];
    }
}
