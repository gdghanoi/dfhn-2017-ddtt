<?php

namespace App;

class Booking extends BaseModel
{
    protected $fillable = [ 'user_id', 'table_id', 'date_time', 'status', 'meal', 'payment_method', 'paid', 'note', ];

    public function foods()
    {
        return $this->belongsToMany(Food::class)->withPivot('id');
    }
}
