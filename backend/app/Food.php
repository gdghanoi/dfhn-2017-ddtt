<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends BaseModel
{
    protected $fillable = [ 'category', 'name', 'price', 'status', 'note'];
}
