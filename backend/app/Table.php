<?php

namespace App;

class Table extends BaseModel
{
    protected $fillable = [ 'status', 'location', 'number_of_seats', 'vip', 'description', 'note', ];

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
