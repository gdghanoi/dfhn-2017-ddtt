<?php

namespace App;

class Restaurant extends BaseModel
{
    protected $fillable = [ 'user_id', 'table_id', 'date_time', 'status', 'payment_method', 'paid', 'note', ];
}
