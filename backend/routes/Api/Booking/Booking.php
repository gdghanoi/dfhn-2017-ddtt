<?php

$api = app('Dingo\Api\Routing\Router');

$api->group(['middleware' => 'jwt.auth'], function ($api) {
  $api->get('/bookings', 'BookingController@index');
  $api->post('/bookings', 'BookingController@store');
  $api->post('/bookings/{id}/handle', 'BookingController@handle');
});