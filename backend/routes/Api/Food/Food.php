<?php

$api = app('Dingo\Api\Routing\Router');

$api->group(['middleware' => 'jwt.auth'], function ($api) {
  $api->get('/foods', 'FoodController@index');
});
