<?php

$api = app('Dingo\Api\Routing\Router');

$api->group(['namespace' => 'Auth', 'as' => 'auth'], function ($api) {
    // Register Route
    $api->post('register', 'AuthenticateController@register')->name('.register');

    // Authentication Routes
    $api->post('authenticate', 'AuthenticateController@authenticate')->name('.authenticate');

    // Logout Route
    $api->get('logout', 'AuthenticateController@logout')->name('.logout');

    // Refresh Token
    $api->get('token', 'AuthenticateController@refreshToken')->name('.token');

    $api->group(['middleware' => 'jwt.auth'], function ($api) {

        // Get logged in user from token in header
        $api->get('me', 'AuthenticateController@getAuthenticatedUser')->name('.me');

        // Check whether token is valid or invalid
        $api->get('check', 'AuthenticateController@checkToken')->name('.check');
    });

});
