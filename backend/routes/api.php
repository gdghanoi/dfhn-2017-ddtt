<?php

use Illuminate\Http\Request;

if (env('APP_ENV') !== 'testing') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: Authorization, Content-Type, X-Requested-With, X-XSRF-TOKEN');
    header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
}
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['namespace' => 'App\Http\Controllers\Api', 'as' => 'api'], function ($api) {
        includeRouteFiles(__DIR__.'/Api/');
    });
});
